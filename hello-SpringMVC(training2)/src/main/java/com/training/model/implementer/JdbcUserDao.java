package com.training.model.implementer;

import com.training.model.dao.UserDao;
import com.training.model.pojo.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


import javax.sql.DataSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class JdbcUserDao implements UserDao {
    private File file = new File("src/main/resources/jdbc.properties");
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    public JdbcUserDao(){
        this.dataSource = getDataSource();
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public DriverManagerDataSource getDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream(file));
            String t = properties.getProperty("postgres.driver");
            dataSource.setDriverClassName(properties.getProperty("postgres.driver"));
            dataSource.setUrl(properties.getProperty("postgres.database.url"));
            dataSource.setUsername(properties.getProperty("postgres.database.username"));
            dataSource.setPassword(properties.getProperty("postgres.database.password"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dataSource;
    }

    @Override
    public void insert(User user) {
        String sql = "INSERT INTO user_table(login, password) VALUES (?, ?);";
        jdbcTemplate.update(sql, user.getName(), user.getPassword());
    }
}
