package com.training.model.dao;

import com.training.model.pojo.User;

public interface UserDao {
    void insert(User user);
}
