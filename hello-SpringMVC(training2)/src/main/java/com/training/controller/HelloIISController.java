package com.training.controller;

import com.training.model.implementer.JdbcUserDao;
import com.training.model.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HelloIISController {
    private JdbcUserDao jdbcUserDao;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String hello(ModelMap model) {
        jdbcUserDao = new JdbcUserDao();
        jdbcUserDao.insert(new User("user1", "password1"));
        model.addAttribute("message", "Hello IIS!");
        return "helloIIS";
    }
}
